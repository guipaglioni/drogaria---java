/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.drogaria.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author heitorap
 */

@Entity
public class Produto extends GenericDomain{
    @Column(length = 50, nullable = false) 
    private String descricao;
    @Column(length = 50, nullable = false) 
    private int quantidade;
    @Column(length = 10, nullable = false) 
    private Double preco;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Fabricante fabricante;
    
    public String getDescricao(){
        return descricao;
    }
    public void setDescricao(String descricao){
        this.descricao = descricao;
    }
    
    public int getQuantidade(){
        return quantidade;
    }
    public void setQuantidade(int quantidade){
        this.quantidade = quantidade;
    }
    
    public Double getPreco(){
        return preco;
    }
    public void setPreco(Double preco){
        this.preco = preco;
    }
    
    public Fabricante getFabricante(){
        return fabricante;
    }
    public void setFabricante(Fabricante fabricante){
        this.fabricante = fabricante;
    }
}
