/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.drogaria.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author heitorap
 */


@Entity
public class Cliente extends GenericDomain{
    
    @Column(length = 50, nullable = false)
    private String dataCadastro;
    @Column(length = 50, nullable = false)
    private Boolean liberado;
    @OneToOne
    @JoinColumn(nullable = false)
    private Pessoa pessoa;
    
    public String getDataCadastro(){
        return dataCadastro;
    }
    public void setDataCadastro(String dc){
        this.dataCadastro = dc;
    }
    
    public Boolean getLiberado(){
        return liberado;
    }
    public void setLiberado(Boolean liberado){
        this.liberado = liberado;
    }
    
    public Pessoa getPessoa(){
        return pessoa;
    }
    public void setPessoa(Pessoa pessoa){
        this.pessoa = pessoa;
    }
}
